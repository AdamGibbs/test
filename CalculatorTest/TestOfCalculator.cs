﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorNS;

namespace CalculatorTest
{
    /// <summary>
    /// Summary description for TestOfCalculator
    /// </summary>
    [TestClass]
    public class TestOfCalculator
    {

        private TestContext testContextInstance;
        
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void AddTest()
        {
            double Result;

            Calculator calculator = new Calculator();
            Result = calculator.Add(10, 5);

            Assert.AreEqual(Result, 15);
        }

        [DataTestMethod]
        [DataRow(1, 1, 2)]
        [DataRow(10, 6, 16)]
        [DataRow(20, 43, 63)]
        [DataRow(70, 360, 430)]

        public void CalculateDataTest(double left, double right, double correct)
        {
            double Result;

            Calculator calculator = new Calculator();
            Result = calculator.Add(left, right);

            Assert.AreEqual(Result, correct);
        }
    }
}
