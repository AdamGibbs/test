﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculatorNS;

namespace CalculatorProject
{
    class Program
    {
        static void Main(string[] args)
        {
            double LeftNumber;
            double RightNumber;
            double Result;

            String LeftInput;
            String RightInput;

            Console.WriteLine("Left Number");
            LeftInput = Console.ReadLine();

            Console.WriteLine("Right Number");
            RightInput = Console.ReadLine();

            LeftNumber = double.Parse(LeftInput);
            RightNumber = double.Parse(RightInput);

            Calculator calculator = new Calculator();
            Result = calculator.Add(LeftNumber, RightNumber);

            Console.WriteLine(Result);

            Console.Read();
        }
    }
}
